import React, { Component } from 'react';

import {
  Route,
  Switch,
  withRouter 
} from 'react-router-dom'


import HomeContainer from 'pages/home/HomeContainer'
import OrderContainer from 'pages/order/OrderContainer'
import MineContainer from 'pages/mine/MineContainer'
import ShopContainer from 'pages/shopdetail/ShopContainer'

// import BottomBar from 'components/common/tabBar/BottomBar';

class App extends Component {
  // style = {
  //   width: '100%',
  //   height: '100%',
  //   display: 'flex',
  //   flexDirection:'column'
  // }
  render() {
    
    return (
      <div style={{ height: '100vh' }}>
        <Switch>
            <Route path="/" component={HomeContainer} exact></Route>
            <Route path="/home" component={HomeContainer}></Route>
            <Route path="/order" component={OrderContainer}></Route>
            <Route path="/mine" component={MineContainer}></Route>
            
            <Route path="/shopdetail/:id" component={ShopContainer}></Route>
        </Switch>    
      </div>
    );
  }
}

export default withRouter(App);
