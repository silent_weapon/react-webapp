import styled from 'styled-components';
import border from 'styles/border.js'

const CommonHead =border({
    component:styled.div`
        width: 100%;
        height: .54rem;
        background: #fff;
        text-align: center;
        line-height: .54rem;
        color: #000;
        font-size: .18rem;
        z-index: 1000;
    `,
    color: '#ccc',
    radius: '0',
    width: '0 0 1px 0'
})


export {
    CommonHead
}