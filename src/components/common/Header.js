import styled from 'styled-components';


const Header = styled.div`
  height: .44rem;
  background: #fff;
  text-align: center;
  line-height: .44rem;
  color: #515151;
  font-size: .16rem;
  z-index:99
`


export {
  Header
}