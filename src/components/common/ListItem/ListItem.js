import './ListItem.scss';

import getHome from 'utils/storeGetter/getHome'

import React,{Component} from 'react';
import { Link, withRouter } from 'react-router-dom';


class ListItem extends Component {
    // constructor(props) {
    //     super(props);
    // }

    componentWillMount() {
        this.props.getShopList()
    }
    /**
     * 渲染是否是新到和品牌标签
     */
    renderBrand(item) {
        if (item.brand_type) {
            return <div className="brand brand-pin">品牌</div>
        } else {
            return <div className="brand brand-xin">新到</div>
        }
    }

    /**
     *  渲染月售数量
     */
    renderMonthNum(item){
        let num = item.month_sale_num;

        // 大于999采用999+
        if (num > 999) {
            return '999+';
        }

        return num;
    }

    /**
     * 是否需要渲染美团专送tag
     */
    renderMeituanFlag(item) {

        if(item.delivery_type) {
            return <div className="item-meituan-flag">美团专送</div>
        }

        return null;
    }

    /**
     * 渲染商家活动
     */
    renderOthers(item) {
        let array = item.discounts2;

        return array.map((item, index)=>{
            return (
                <div key={index} className="other-info">
                    <img src={item.icon_url} className="other-tag" alt=""/>
                    <div className="other-content">{item.info}</div>
                </div>
            )
        });

    }
    
    goDetail(item){
        console.log(111)
        this.props.history.push(`/shopdetail/:${item.id}`)
    }

    render(){
        
        let {item} = {...this.props}
        return (
            <Link to={`/shopdetail/:${item.id}`} onClick={() => this.goDetail(item)} key={item.id} className="r-item-content scale-1px">
                    <img className="item-img" src={item.pic_url} alt="" />
                    {this.renderBrand(item)}
                    <div className="item-info-content">
                        <p className="item-title">{item.name}</p>
                        <div className="item-desc clearfix">
                            <div className="item-score">评分 {item.wm_poi_score}</div>
                            <div className="item-count">月售 {this.renderMonthNum(item)}</div>
                            <div className="item-distance">&nbsp;{item.distance}</div>
                            <div className="item-time">{item.mt_delivery_time}&nbsp;|</div>
                        </div>
                        <div className="item-price">
                            <div className="item-pre-price">{item.min_price_tip}</div>
                            {this.renderMeituanFlag(item)}
                        </div>
                        <div className="item-others">
                            {this.renderOthers(item)}
                        </div>
                    </div>
                </Link>
        );
    }
}
export default withRouter(getHome(ListItem)) 
