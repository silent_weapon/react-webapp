import './BottomBar.scss';

import React, { Component } from 'react'

import { NavLink, withRouter } from 'react-router-dom';



class BottomBar extends Component {
    // constructor(props) {
    //     super(props)
    // }
    
    render() {
        return (
            <div className="bottom-bar">
                <NavLink to='/home' activeClassName="active" className='btn-item' >
                    <div className="tab-icon home">   </div>
                    <div className="btn-name">首页</div>
                </NavLink>

                <NavLink to='/order' activeClassName="active" className='btn-item' >
                    <div className="tab-icon order"></div>
                    <div className="btn-name">订单</div>
                </NavLink>

                <NavLink to='/mine' activeClassName="active" className='btn-item' >
                    <div className="tab-icon  my"></div>
                    <div className="btn-name">我的</div>
                </NavLink>
            </div>
        )
    }
}

export default  withRouter(BottomBar)