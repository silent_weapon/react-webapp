import React, { Component } from 'react'

import { Grid } from 'antd-mobile';
import getHome from 'utils/storeGetter/getHome'


class HomeCategory extends Component{
    constructor(props){
        super(props);
    
    }
    componentWillMount() {
        this.props.getHead()
    }
   
    
    render (){
        let data = this.props.head && this.props.head.map((_val,i) => ({
            icon: _val.url,
            text:_val.name
        }));
         return(
             <Grid data={data}
                 hasLine={false}
                 columnNum={4}
                 renderItem={dataItem => (
                     <div style={{ padding: '12px' }}>
                         <img src={dataItem.icon} style={{ width: '40px', height: '40px' }} alt="" />
                         <div style={{ color: '#888', fontSize: '14px', marginTop: '10px' }}>
                             <span>{dataItem.text}</span>
                         </div>
                     </div>
                 )}
             />
            )
    } 
}


export default getHome(HomeCategory) 
