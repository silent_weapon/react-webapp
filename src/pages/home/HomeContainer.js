import React, { Component } from 'react';

import dwIcon from 'images/dwIcon.png';
import msgIcon from 'images/msgIcon.png'
import weatherIcon from 'images/weatherIcon.png'
import right from 'images/right.png'
// import styled from 'styled-components';

import BottomBar from 'components/common/tabBar/BottomBar';
import { Header } from 'components/common/Header'
import { SearchContainer, SearchContent } from 'components/common/Search'
import HomeCategory from 'pages/home/HomeCategory'
import ShopList from 'pages/home/ShopList'

import searchIcon from 'images/searchIcon.png'

import BScroll from 'better-scroll'


class HomeContainer extends Component {
  style = {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#f2f1ef',
    justifyContent: 'center'
  }
  style2 = {
    backgroundColor: 'white',
    flex: '1'
  }
  componentDidMount() {
    const scroll = new BScroll('.wrapper')

  }

  render() {
    return (

      <div style={this.style2} style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
        <Header>
          <div style={{ display: 'flex', justifyContent:'space-between', height:'100%',width:'100%'}}>
          <div style={{padding:'1px 0 0 10px'}}>
              <img src={dwIcon} alt=" " style={{ width: '.23rem', height: '.25rem', lineHeight: '.44rem', display: 'inline-block', marginRight: '.05rem'  }}/>
              <span>杭州旺田大酒店</span>
              <img src={right} alt=" " style={{ width: '.12rem', height: '.12rem'}} />
          </div>
          
          <div style={{ padding: '1px 10px  0 0px' }}>
            <img src={msgIcon} alt=" " style={{ width: '.25rem',display:'inline-block',marginRight:'.1rem' }} />
            <img src={weatherIcon} alt=" " style={{ width: '.3rem' }} />
          </div>
          </div>
        </Header>

        <div className="wrapper" style={{ height: '100%', flex: '1' ,background:'#fff'}}>
          <div>
            <SearchContainer>
              <SearchContent style={this.style}>
                <img src={searchIcon} alt="搜索" />
                <span>德克士  满35减15</span>
              </SearchContent>
            </SearchContainer>

            <HomeCategory></HomeCategory>

            <ShopList></ShopList>
          </div>
          
        </div>
        
        <BottomBar {...this.props}></BottomBar>
      </div>

    );
  }
}

export default HomeContainer