import './ShopList.scss';
import React, { Component } from 'react';
// import { Link, withRouter } from 'react-router-dom';

import ListItem from 'components/common/ListItem/ListItem.js';
import getHome from 'utils/storeGetter/getHome'


class ShopList extends Component {
    constructor(props) {
        super(props);
       
    }
    componentWillMount() {
        this.props.getShopList()
    }


    /* renderItems() {
        let list = this.props.list;
        return list.map((item, index) => {
            return <ListItem></ListItem>
        });
    } */


    /* handleClick(){
        console.log('clk')
    } */

    // <Link to={`/shopdetail/:${item.id}`} key={item.id}></Link>  没用
    render() {
        return (
            <div className="list-content">
                <h4 className="list-title">
                    <span className="title-line"></span>
                    <span>附近商家</span>
                    <span className="title-line"></span>
                </h4>
                <div style={{paddingBottom:'.5rem'}}>
                    {!this.props.shoplist?'' : this.props.shoplist.map((item)=>{
                        return  <ListItem key={item.id} item={item}  />
                    })}
                </div>
            </div>
        );
    }
}

export default getHome(ShopList) 