import React, { Component } from 'react';

import BottomBar from 'components/common/tabBar/BottomBar';
import MineHeader from 'pages/mine/MineHeader'
import MineContent from 'pages/mine/MineContent'


class MineContainer extends Component {
    render() {
        return (
            <div className="my">
                <MineHeader></MineHeader>
                <MineContent></MineContent>
                <BottomBar {...this.props}></BottomBar>
            </div>
        );
    }
}

export default MineContainer;
