import React, { Component } from 'react';

import { List } from 'antd-mobile';


const Item = List.Item;
// const Brief = Item.Brief;

class MineContent extends Component {
    render() {
        return (
            <div className="maincontent">
                <List className="my-list">
                    <Item arrow="horizontal" onClick={() => { }}>收货地址管理</Item>
                    <Item arrow="horizontal" onClick={() => { }}>商家代金券</Item> 
                </List>

                <List renderHeader={() => ''} className="my-list">
                    <Item arrow="horizontal" onClick={() => { }}>意见反馈</Item>
                    <Item arrow="horizontal" onClick={() => { }}>常见问题</Item>            
                </List>

                <List renderHeader={() => ''} className="my-list">
                    <Item>客服电话:&nbsp;101-097-77</Item>
                    <Item>常见问题</Item>
                </List>
            
            </div>
        );
    }
}

export default MineContent;
