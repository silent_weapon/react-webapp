import React, { Component } from 'react';

import headerbg from 'images/header.png'

class MineHeader extends Component {
    headerstyle = {
        width: '100%',
        height: '1.8rem',
        background: `url(${headerbg}) 0 0`,
        backgroundSize:'cover',
        overflow:'hidden'
    }
    imgstyle={
        width: '.72rem',
        height: '.72rem',
        margin: '0 auto',
        display: 'block',
        marginTop: '.25rem',
        border: '3px solid rgba(255, 255, 255, 0.4)',
        borderRadius: '50%'
    }
    pstyle={
        color: '#333',
        fontSize:'.16rem',
        textAlign: 'center',
        marginTop:'.15rem'
    }
    render() {
        return (
            <div className="mineHeader" style={this.headerstyle}>
                <img className="avatar" style={this.imgstyle} alt="" src="http://i.waimai.meituan.com/static/img/default-avatar.png" />
                <p className="nickname" style={this.pstyle}>都怪宇宙  &gt;</p>
            </div>
        );
    }
}

export default MineHeader;
