import React, { Component } from 'react';
import OrderList from './OrderList';
import { CommonHead } from 'components/common/CommonHead';
import BottomBar from 'components/common/tabBar/BottomBar';

import getOrder from 'utils/storeGetter/getOrder'

import BScroll from 'better-scroll'



class OrderContainer extends Component {
    // constructor(props) {
    //     super(props);

    // }
    
    componentWillMount() {
        this.props.getOrderList()
    }
    componentDidMount(){
        const scroll = new BScroll('.wrapper')

    }

    render() {
        return (
            <div className='orderContainer' style={{ height: '100%',display:'flex',flexDirection:'column'}}>
                <CommonHead>订单</CommonHead>
                <div className="wrapper" style={{height:'100%',flex:'1'}}>
                    <div className="content">
                        {!this.props.orderlist? '' : this.props.orderlist.map(item=>{
                        return <OrderList key={item.order_id} item={item}></OrderList>
                        })} 
                    </div>
                       
                </div>

                <BottomBar {...this.props}></BottomBar>
                
            </div>
        );
    }
}

export default getOrder(OrderContainer);
