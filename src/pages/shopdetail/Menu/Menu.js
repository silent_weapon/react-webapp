import './Menu.scss';

import React, { Component } from 'react';

import MenuItem from './MenuItem/MenuItem';
// import ShopBar from './ShopBar/ShopBar';

import getDetail from 'utils/storeGetter/getDetail'

class Menu extends Component {
    constructor(props){
        super(props);
        // console.log(this.props)
    }

    componentDidMount (){
        this.props.getMenu()
    }

    /* 右侧列表 */
    /* renderRightList(array) {
        let _array = array || [];
        return _array.map((item, index) => {
            if (!item.chooseCount) {
                item.chooseCount = 0;
            }
            return <MenuItem key={index} data={item} _index={index}></MenuItem>
        });
    } */

    /* 点击切换右侧列表  没写！！！！！ */
    /* itemClick(index) {
        this.props.dispatch(itemClick({
            currentLeftIndex: index
        }));
    } */

    /* 渲染右侧列表  没写！！！！！！！！ */
    /* renderRight() {
        let index = this.props.currentLeftIndex;
        let array = this.props.listData.food_spu_tags || [];
        let currentItem = array[index];

        if (currentItem) {
            let title = <p key={1} className="right-title">{currentItem.name}</p>
            return [
                title,
                <div key={2} className="right-list"><div className="right-list-inner">{this.renderRightList(currentItem.spus)}</div></div>
            ]
        } else {
            return null;
        }
    } */

    /* 渲染左侧列表  没写！！！！！！！！！！*/
    /* renderLeft() {
        let list = this.props.listData.food_spu_tags || [];

        return list.map((item, index) => {
            let cls = this.props.currentLeftIndex === index ? 'left-item active' : 'left-item';
            return (
                <div onClick={() => this.itemClick(index)} key={index} className={cls}>
                    <div className="item-text">{item.icon ? <img className="item-icon" src={item.icon} /> : null}{item.name}</div>
                </div>
            );
        });
    } */

    render() {
        // console.log(this.props)
        if(this.props.menu){
            console.log('menu拿到数据了~')
            return (
                <div className="menu-inner">
                    <div className="left-bar">
                        <div className="left-bar-inner">
                            左边
                        </div>
                    </div>
                    <div className="right-content">
                        {!this.props.menu ? '' : this.props.menu.map((item) => {
                            return <MenuItem key={item.id} item={item} />
                        })}
                    </div>
                </div>
            );
        } else {
            return (<div />)
        }
        
    }
}

export default getDetail(Menu);
