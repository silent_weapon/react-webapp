import './MenuItem.scss';

import React, { Component } from 'react'; 
import getDetail from 'utils/storeGetter/getDetail'

class MenuItem extends Component {
    componentDidMount() {
        this.props.getMenu()
        // console.log(this.props) 
    }


    render() {
            // let item = this.props.menu && this.props.menu

            let { item } = { ...this.props }
            // console.log(item)
            return (
                <div className="menu-item" key={item.id}>
                    <img className="img" src={item.spus[0].picture} />
                    <div className="menu-item-right">
                        <p className="item-title">{item.spus[0].name}</p>
                        <p className="item-desc two-line">{item.spus[0].description}</p>
                        <p className="item-zan">{item.spus[0].praise_content}</p>
                        <p className="item-price">¥{item.spus[0].min_price}<span className="unit">/{item.unit}</span></p>
                    </div>
                </div>
            );
       
    }
}

export default getDetail(MenuItem);
