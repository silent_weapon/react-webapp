
import './ShopMain.scss';

import React from 'react';

import { NavBar, Icon } from 'antd-mobile';

import { Route, withRouter, NavLink } from 'react-router-dom';

import Menu from './Menu/Menu';
import Comment from './Comment/Comment';
import Restaurant from './Restaurant/Restaurant';


class ShopMain extends React.Component {
    constructor(props,context) {
        super(props,context);
        console.log(props)
        // this.shopdetailstate=props.location.state.data

    }
    
    
    render() {
        // let poiName = this.props.poiInfo.poi_info ? this.props.poiInfo.poi_info.name : '';
        return (
            <div className="detail">
                
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => {
                        // console.log(this.props)
                        this.props.history.go(-1)
                    }}
                >店名啊啊啊啊啊</NavBar>

                <div className="tab-bar">
                    <NavLink to='/shopdetail/menu' activeClassName="active" className='tab-item' >
                            点菜
                    </NavLink>

                    <NavLink to='/shopdetail/comment' activeClassName="active" className='tab-item' >
                            评价
                    </NavLink>

                    <NavLink to='/shopdetail/restaurant' activeClassName="active" className='tab-item' >
                            商家
                    </NavLink>
                </div>
                <div>
                    <Route exact path="/shopdetail/menu" component={Menu} />
                    <Route exact path="/shopdetail/comment" component={Comment} />
                    <Route exact path="/shopdetail/restaurant" component={Restaurant} />
                    {this.props.showChooseContent ? <div className="mask"></div> : null}
                </div>
                
            </div>
        );
    }
}

// export default withRouter(connect(
//     state => ({
//         tabs: state.tabReducer.tabs,
//         showChooseContent: state.menuReducer.showChooseContent,
//         poiInfo: state.menuReducer.poiInfo
//     })
// )(Main));

export default ShopMain;