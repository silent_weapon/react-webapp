
import * as type from './types'

const actionCreators = {
    getHead(){

        /* 异步请求 */
        return dispatch=>{
            fetch('/data/head.json')
            .then(res=>res.json())
            .then(result=>{
                // console.log(result)
                let action ={
                    type: type.GET_HOME_HEAD,
                    payload: result.data.primary_filter.slice(0,8)
                }
                dispatch( action )
            })
            .catch(error => console.log(error))
        }
    },

    getShopList() {

        /* 异步请求 */
        return dispatch => {
            fetch('/data/list.json')
                .then(res => res.json())
                .then(result => {
                    let action = {
                        type: type.GET_HOME_SHOPLIST,
                        payload: result.data.poilist
                    }
                    // console.log(action.payload)
                    dispatch(action)
                })
                .catch(error => console.log(error))
        }
    }

    
}

export default actionCreators