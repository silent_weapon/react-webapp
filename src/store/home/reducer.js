/* 纯函数  修改数据 */

import state from "./state";
import * as type from './types'

const reducer =(previousState=state,action)=> {
    let new_state={...previousState}

    switch (action.type) {
        case type.GET_HOME_HEAD:
            new_state.head = action.payload
            break;
    
        case type.GET_HOME_SHOPLIST:
            new_state.shoplist = action.payload
            break;

        default:
            break;
    }

    return new_state
}

export default reducer