
import * as type from './types'

const actionCreators = {
   
    getOrderList() {

        /* 异步请求 */
        return dispatch => {
            fetch('/data/orders.json')
                .then(res => res.json())
                .then(result => {
                    // console.log(result.data.digestlist)
                    let action = {
                        type: type.GET_ORDER_LIST,
                        payload: result.data.digestlist
                    }
                    
                    dispatch(action)
                })
                .catch(error => console.log(error))
        }
    }
}

export default actionCreators