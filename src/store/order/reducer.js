/* 纯函数  修改数据 */

import state from "./state";
import * as type from './types'

const reducer =(previousState=state,action)=> {
    let new_state={...previousState}

    switch (action.type) {
        case type.GET_ORDER_LIST:
            new_state.orderlist = action.payload
            break;
    

        default:
            break;
    }

    return new_state
}

export default reducer