
/* 
 统一的reducer
*/

import { combineReducers } from 'redux'

import home from './home/reducer'
import order from './order/reducer'
// import mine from './mine/reducer'
import shopdetail from './shopdetail/reducer'

const reducer = combineReducers({
    home,
    order,
    shopdetail
})

export default reducer