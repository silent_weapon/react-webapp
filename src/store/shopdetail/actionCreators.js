import * as type from './types'

const actionCreators={

    /* get menu */
    getMenu(){
        return dispatch=>{
            fetch('/data/food.json')
            .then(res=>res.json())
            .then(result=>{
                // console.log(result.data)
                let action={
                    type:type.GET_DETAIL_MENU,
                    payload: result.data.food_spu_tags
                }
                dispatch(action)
            })
            .catch(error=>console.log(error))
        }
    }

}

export default actionCreators