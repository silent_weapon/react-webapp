import state from "./state";
import * as type from './types'

const reducer=(previousState=state,action)=>{
    let new_state={...previousState}

    switch(action.type){
        case type.GET_DETAIL_MENU:
            new_state.menu = action.payload
            break;

        default:
            break;
    }

    return new_state
}

export default reducer