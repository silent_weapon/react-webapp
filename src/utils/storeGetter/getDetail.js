import { connect } from 'react-redux'
import actionCreators from 'store/shopdetail/actionCreators'

import { bindActionCreators } from 'redux'

const getDetail = (UIComponent) => {
    return connect(
        state => state.shopdetail,
        dispatch => {
            return bindActionCreators(actionCreators, dispatch)
        }
    )(UIComponent)
}

export default getDetail