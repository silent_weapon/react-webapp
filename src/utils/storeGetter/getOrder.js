import {connect} from 'react-redux'
import actionCreators from 'store/order/actionCreators'

import {bindActionCreators} from 'redux'

const getOrder =(UIComponent)=>{
    return connect(
        state => state.order,
        dispatch => {
             return bindActionCreators(actionCreators,dispatch)
        }
    )(UIComponent)
}

export default getOrder